var clockMode = "time";
var timeout;
var format = "%H:%M:%S";
var stopwatchTime = {
  hund: 0,
  seconds: 0,
  minutes: 0,
  hours: 0,
}
var staticTime = {
  hund: 0,
  seconds: 0,
  minutes: 0,
  hours: 0,
}

function setMode(mode) {
  clockMode = mode;
}


var clockModes = {
  "time": function() {
    let d = new Date();
    return date_to_time(d);
  },
  "stopwatch-on": function() {
    if (++stopwatchTime.hund%100 == 0) {
      if (++stopwatchTime.seconds%60 == 0) {
        if (++stopwatchTime.minutes%60 == 0) {
          stopwatchTime.hours++;
        }
      }
    }

    return stopwatchTime;
  },
  "stopwatch-off": function() {
    return staticTime;
  }
}

function clockValue() {
  let value = clockModes[clockMode]();
  return value;
}

// Normal Clock / Current Time

function setClock() {
    timeout = setTimeout(setClock, 1000/100);
    let value = clockValue();
    let time = formatTime(value, format);
    get("time").innerHTML = time;
    get("title").innerHTML = time;
}

// Stopwatch

function startStopwatch() {
  hide("btn__start");
  show("btn__stop");
  setMode("stopwatch-on");
}

function stopStopwatch() {
  show("btn__start");
  hide("btn__stop");
  staticTime = stopwatchTime;
  setMode("stopwatch-off");

}

function resetStopwatch() {
  stopwatchTime = {
    hund: 0,
    seconds: 0,
    minutes: 0,
    hours: 0,
  }
  staticTime = {
    hund: 0,
    seconds: 0,
    minutes: 0,
    hours: 0,
  }
  clearTimeout(timeout);
  setClock();
}
