# Time
time is an opensouce web clock.


Uses https://github.com/js-cookie/js-cookie (MIT lisence for project is [here](https://raw.githubusercontent.com/js-cookie/js-cookie/master/LICENSE)) for cookies

Uses http://jscolor.com/ (released for opensouce projects with GPL3 compatable lisences under GPL3) for a cross platform color picker
